package com.smartbics.logs;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

    private final static String LOG_TYPE = "ERROR";
    private final static int LOGS_DIR_PATH_IDX = 0;
    private final static int AGGREGATION_TYPE_IDX = 1;
    private final static DateTimeFormatter dateF = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    // Predicates
    private final static Predicate<Path> onlyLogFiles = p -> !p.toFile().isDirectory() && p.toString().toLowerCase().endsWith(".log");
    private final static Predicate<String[]> onlyError = l -> l.length > 1 && l[1].trim().equals(LOG_TYPE);

    public static void main(String[] args) {
        final var t0 = System.nanoTime();
        final var logsDir = getLogsDirPath(args);
        final var aggrType = getAggregationType(args);
        final var resultFile = Paths.get(logsDir.getParent().toString() + File.separator + "result.txt");

        Map<String, Integer> errors = new ConcurrentHashMap<>();

        try (Stream<Path> files = Files.list(logsDir)) {
            files.filter(onlyLogFiles)
                .forEach((path) -> {
                    try (Stream<String> lines = Files.lines(path)) {
                        lines.filter(l -> !l.isBlank())
                            .map(line -> line.split(";")).filter(onlyError)
                            .forEach(l -> {
                                if (errors.containsKey(formatDateTime(l[0], aggrType))) {
                                    errors.computeIfPresent(formatDateTime(l[0], aggrType), (k, v) -> ++v);
                                } else errors.putIfAbsent(formatDateTime(l[0], aggrType), 1);
                            });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        } catch (NoSuchFileException e){
            System.out.println(String.format("Directory '%s' not found", logsDir.toString()));
        } catch (IOException e){
            System.out.println("Error: " + e.getMessage());
        }

        final var t1 = System.nanoTime();
        final var millisRun = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
        System.out.println(String.format("\n- Run time: %d ms", millisRun));

        final var t3 = System.nanoTime();
        Map<String, Integer> sortedErrors = getSortedErrors(errors);

        final var t4 = System.nanoTime();
        final var millisSorting = TimeUnit.NANOSECONDS.toMillis(t4 - t3);
        System.out.println(String.format("- Sorting time: %d ms\n", millisSorting));

        saveResult(sortedErrors, resultFile);
    }

    private static String formatDateTime(String dateTime, AggregationTypeEnum aggrType) {
        LocalDateTime localDateTime = LocalDateTime.parse(dateTime);

        if (aggrType == AggregationTypeEnum.MINUTE) {
            return String.format("%s, %s-%s", localDateTime.format(dateF), localDateTime.format(aggrType.getFormatter()),
                    localDateTime.plusMinutes(1).format(aggrType.getFormatter()));
        } else {
            return String.format("%s, %s-%s", localDateTime.format(dateF), localDateTime.format(aggrType.getFormatter()),
                    localDateTime.plusHours(1).format(aggrType.getFormatter()));
        }
    }

    private static Path getLogsDirPath(String[] args) {
        if (args.length < 1 || args[LOGS_DIR_PATH_IDX] == null) {
            return Paths.get(System.getProperty("user.dir") + "/logs");
        } else return Paths.get(args[LOGS_DIR_PATH_IDX]);
    }

    private static AggregationTypeEnum getAggregationType(String[] args) {
        AggregationTypeEnum aggrType;
        try {
            aggrType = AggregationTypeEnum.valueOf(args[AGGREGATION_TYPE_IDX]);
        } catch (Exception e) {
            aggrType = AggregationTypeEnum.HOUR;
        }
        return aggrType;
    }

    private static Map<String, Integer> getSortedErrors(Map<String, Integer> errors) {
        return errors
                .entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }

    private static void saveResult(Map<String, Integer> sortedErrors, Path resultFile) {
        var f = new File(resultFile.toString());

        try {
            if (!Files.exists(resultFile)) Files.createFile(resultFile);
            else Files.writeString(resultFile, "", StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            System.out.println("Could not create result file: " + e.getMessage());
        }
        sortedErrors.forEach((k, v) -> {
            try {
                Files.writeString(resultFile, String.format("%s Количество ошибок: %s\n", k, v), StandardOpenOption.APPEND);
            } catch (IOException e) {
                System.out.println("Could not save result file: " + e.getMessage());
            }
        });
    }
}
