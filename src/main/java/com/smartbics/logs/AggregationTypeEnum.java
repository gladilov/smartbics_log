package com.smartbics.logs;

import java.time.format.DateTimeFormatter;

public enum AggregationTypeEnum {
    DATE("yyyy-MM-dd"),
    HOUR("HH:00"),
    MINUTE("HH:mm");

    private DateTimeFormatter formatter;

    AggregationTypeEnum(String string) {
        this.formatter = DateTimeFormatter.ofPattern(string);
    }

    public DateTimeFormatter getFormatter() {
        return formatter;
    }
}
